const gulp = require('gulp');
const sass = require('gulp-sass');
function style() {
  return gulp.src('./sass/*.scss')
    // .pipe(sass({outputStyle: 'compressed'}).on('error',sass.logError))
    .pipe(sass({outputStyle: 'expanded'}).on('error',sass.logError))
    .pipe(gulp.dest('./css'))
}
function watch() {
  gulp.watch('./sass/*.scss', style)
}
exports.style = style;
exports.watch = watch;